A fork of the original Youtube Unhooked, without the feature of removing comments.

The content of the original README follows:

# youtube-feed-hider
A Chrome extension for hiding the feed on Youtube.com

Install here:
https://chrome.google.com/webstore/detail/fkhfakakdbjcdipdgnbfngaljiecclaf/

Also see this related extension from Stanford:
https://habitlab.stanford.edu/
